rally-openstack (3.0.0-3) unstable; urgency=medium

  * Add install-all-files.patch (Closes: #1093141).
  * Move config.ini to /etc/rally and symlink it.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Jan 2025 18:18:25 +0100

rally-openstack (3.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090688).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 14:48:38 +0100

rally-openstack (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Removed muranoclient (build-)depends.
  * Drop remove-gnocchiclient-constraint.patch.
  * Drop use-resolvectl-not-systemd-resolve.patch applied upstream.
  * Add --ignore tests/unit/test_pytest_launcher.py when running tests.
  * Ignore 4 failing tests.

 -- Thomas Goirand <zigo@debian.org>  Wed, 11 Dec 2024 08:51:49 +0100

rally-openstack (2.2.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1049305).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 15:24:48 +0200

rally-openstack (2.2.0-2) unstable; urgency=medium

  * Add use-resolvectl-not-systemd-resolve.patch (Closes: #990495).

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Dec 2021 18:13:21 +0100

rally-openstack (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Removed python3-ceilometerclient from (build-)depends.
  * Test with installed package.
  * Use -W ignore::pytest.PytestCollectionWarning when running tests
    (Closes: #997536).
  * Add remove-gnocchiclient-constraint.patch. 

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Nov 2021 10:52:16 +0100

rally-openstack (2.0.0-2) unstable; urgency=medium

  * d/rules: Do not call "dh_python3 /usr/share" (Closes: #985053).

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Mar 2021 15:43:44 +0100

rally-openstack (2.0.0-1) unstable; urgency=medium

  * New upstream release:
    - Fix FTBFS (Closes: #975823).
  * Removed remove-failing-py38-tests.patch.
  * Fixed (build-)depends for this release.
  * Add disable-some-failing-lists.patch.
  * Switch to debhelper-compat.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Oct 2020 09:03:36 +0200

rally-openstack (1.7.0-2) unstable; urgency=medium

  * Re-upload source-only.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Sep 2020 17:37:05 +0200

rally-openstack (1.7.0-1) unstable; urgency=medium

  * Initial release. (Closes: #917625)

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Dec 2018 16:40:51 +0100
